<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\transaction;
use Illuminate\Database\Eloquent\SoftDeletes;

use function GuzzleHttp\Promise\all;

class ControllerTransaction extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transacs = transaction::all();
        return view('transactions', compact('transacs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('NewTransaction');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaction = new transaction();
        $transaction->cpf = $request->input('cpf');
        $transaction->status = $request->input('status');
        $transaction->user = auth()->user()->name;
        $transaction->valor = $request->input('valor');
        $transaction->save();
        return redirect('/transactions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transac = transaction::find($id);
        if(isset($transac)){
          return view('editTransaction', compact('transac') );
        }
        return redirect('/transactions');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transac = transaction::find($id);
        if(isset($transac)){
            $transac->cpf = $request->input('cpf');
            $transac->status = $request->input('status');
            $transac->user = auth()->user()->name;
            $transac->valor = $request->input('valor');
            
            $transac->save();

        }
        return redirect('/transactions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transac = transaction::find($id);
        if(isset($transac)){
            $transac->delete();
        }
        return redirect('/transactions');
    }
}
