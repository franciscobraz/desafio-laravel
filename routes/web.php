<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/transactions','ControllerTransaction@index');
Route::post('/transactions','ControllerTransaction@store');
Route::post('/transactions/{id}','ControllerTransaction@update');
Route::get('/transactions/create','ControllerTransaction@create');
Route::get('/transactions/delete/{id}','ControllerTransaction@destroy');
Route::get('/transactions/edit/{id}','ControllerTransaction@edit');

Auth::routes();

Route::get('/home', 'ControllerTransaction@index')->name('home');
