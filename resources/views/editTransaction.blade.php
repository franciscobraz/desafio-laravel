@extends('layout.app', ["current" =>"transactions"])
@section('body')
   <div class="card border">
      <div class="card-body">
         <form action="/transactions/{{$transac->id}}" method="POST">
            @csrf
            <div class="form-group">
               <label for="cpf">CPF</label>
               <input type="text" class="form-control" name="cpf" id="cpf" placeholder="CPF" value="{{$transac->cpf}}">

               <div class="form-group">
                  <label for="status">Status</label>
                  <select class="form-control" id="status" name="status" value="{{$transac->status}}">
                    <option>Em Processamento</option>
                    <option>Aprovada</option>
                    <option>Negada</option>
                  </select>
                </div>
                
               <label for="valor">Valor</label>
               <input type="text" class="form-control" name="valor" id="valor" placeholder="Valor" value="{{$transac->valor}}">
            </div>

            <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
            <a href="/transactions" class="btn btn-danger btn-sm">voltar</a>
         </form>
      </div>
   </div>
@endsection