@extends('layout.app', ["current" =>"transactions"])
@section('body')
   <div class="card border">
      <div class="card-body">
         <h5 class="card-title">Listagem de Transações <a href="/transactions/create" class="btn btn-sm btn-primary">Nova Transação</a></h5>
         @if (isset($transacs))
            <table class="table table-ordered table-hover">
               <thead>
                  <tr>
                     <th>Data da transação</th>
                     <th>Usuário</th>
                     <th>CPF</th>
                     <th>Valor</th>
                     <th>Status</th>
                     <th>Ações</th>
                  </tr>
               </thead>
               <tbody>
                  @foreach ($transacs ?? '' as $transaction)
                     <tr>
                        <td>{{$transaction->created_at->format('d-m-yyyy')}}</td>
                        <td>{{auth()->user()->name}}</td>
                        <td>{{$transaction->cpf}}</td>
                        <td>R$ {{$transaction->valor}}</td>
                        <td>{{$transaction->status}}</td>

                        <td>
                           <a href="/transactions/edit/{{$transaction->id}}" class="btn btn-sm btn-primary">Editar</a>
                           <a href="/transactions/delete/{{$transaction->id}}" class="btn btn-sm btn-danger">Apagar</a>
                        </td>
                     </tr>
                  @endforeach
               </tbody>
            </table>
         @endif  

      </div>
      
   </div>
@endsection