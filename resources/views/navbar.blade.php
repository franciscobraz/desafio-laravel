<nav class="navbar navbar-expand-lg navbar-dark bg-dark rounded">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbar">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li @if($current=="transactions") class="nav-item active" @else class="nav-item" @endif>
        <a class="nav-link" href="/transactions">Transações <span class="sr-only">Transações</span></a>
          </li>
      </ul>
    </div>
  </nav>